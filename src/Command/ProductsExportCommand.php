<?php

namespace App\Command;

use App\Entity\Product;
use App\Repository\ProductRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProductsExportCommand extends Command
{
    protected static $defaultName = 'products:export';
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Export products')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Limit', 100)
            ->addArgument('filename', InputArgument::OPTIONAL, 'Excel filename', 'export.xlsx');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $limit = $input->getArgument('limit');
        $filename = $input->getArgument('filename');

        /* @var \Doctrine\Bundle\DoctrineBundle\Registry $em */
        $em = $this->container->get('doctrine')->getManager();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        /* @var ProductRepository $productRepository */
        $productRepository = $em->getRepository(Product::class);

        $sheet->setCellValue("A1", 'Id');
        $sheet->setCellValue("B1", 'Name');
        $sheet->setCellValue("C1", 'Size');
        $sheet->setCellValue("D1", 'Price');
        $sheet->setCellValue("E1", 'Type');
        $sheet->setCellValue("F1", 'Manufacturer');
        $sheet->setCellValue("G1", 'Material');
        $sheet->setCellValue("H1", 'Stock');
        $sheet->setCellValue("I1", 'Certificate');
        $sheet->setCellValue("J1", 'UpdatedAt');

        $i = 2;

        foreach ($productRepository->lastUpdated($limit) as $product) {
            $sheet->setCellValue("A{$i}", $product->getId());
            $sheet->setCellValue("B{$i}", $product->getName());
            $sheet->setCellValue("C{$i}", $product->getSize());
            $sheet->setCellValue("D{$i}", $product->getPrice());
            $sheet->setCellValue("E{$i}", $product->getType());
            $sheet->setCellValue("F{$i}", $product->getManufacturer());
            $sheet->setCellValue("G{$i}", $product->getMaterial());
            $sheet->setCellValue("H{$i}", $product->getStock());
            $sheet->setCellValue("I{$i}", $product->getCertificate());
            $sheet->setCellValue("J{$i}", $product->getUpdatedAt());
            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($filename);

        $io->success("Saved to {$filename}.");
    }

}
