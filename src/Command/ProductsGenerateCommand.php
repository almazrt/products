<?php

namespace App\Command;

use App\Entity\Product;
use DateInterval;
use PhpParser\JsonDecoder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ProductsGenerateCommand extends Command
{
    protected static $defaultName = 'products:generate';
    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setDescription('Generate products')
            ->addArgument('url', InputArgument::OPTIONAL, 'JSON data url', 'http://www.json-generator.com/api/json/get/cqtfgrgfnm?indent=2');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $url = $input->getArgument('url');

        $jsonDecode = new JsonDecode();
        $productsData = $jsonDecode->decode(file_get_contents($url), JsonEncoder::FORMAT);

        /* @var \Doctrine\Bundle\DoctrineBundle\Registry $em */
        $em = $this->container->get('doctrine')->getManager();

        foreach ($productsData as $data) {

            $product = new Product();
            $product->setName($data->name);
            $product->setSize($data->size);
            $product->setPrice($data->price);
            $product->setType($data->type);
            $product->setManufacturer($data->manufacturer);
            $product->setMaterial($data->material);
            $product->setStock($data->stock);
            $product->setCertificate($data->certificate);
            $now = new \DateTime("now");
            $product->setUpdatedAt($now->add(new DateInterval('PT' . rand(1, 24) . 'H')));

            $em->persist($product);
            $em->flush();

        }

        $io->success('Inserted ' . count($productsData) . ' products.');
    }

}
